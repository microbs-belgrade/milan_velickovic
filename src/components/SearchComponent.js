import { useState, useEffect } from "react";
import styled from "styled-components";
import useDebounce from "../hooks/useDebounce";

const Wrapper = styled.div`
  width: 350px;
  margin: auto;
`;

const SearchInput = styled.input`
  width: 100%;
  box-sizing: border-box;
  padding: 5px;
  border-radius: var(--border-radius);
  border: 1px solid var(--border-color);
  outline: none;

  &::-webkit-search-cancel-button {
    -webkit-appearance: none;
  }
`;

const ResultsList = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 10px;
  background-color: lightgray;
  list-style: none;
  padding: 10px 15px;
  margin-top: 10px;
  border-radius: var(--border-radius);
  border: 1px solid var(--border-color);
`;

const ListItem = styled.li`
  width: 100%;
  background-color: white;
  border-radius: var(--border-radius);
  border: 1px solid var(--border-color);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 10px;
  box-sizing: border-box;
`;

const DEFAULT_ITEMS = ["A", "B", "C", "D", "E"];
const URL = "https://google-search3.p.rapidapi.com/api/v1/search";
// In a real project, api key can be moved to .env files
const OPTIONS = {
  headers: {
    "X-RapidAPI-Key": "9d4a5f2e93msh086e635bc640654p1f5cb3jsna152b001252e",
  },
};
const SEARCH_DELAY = 1000;

const SearchComponent = () => {
  const [searchResults, setSearchResults] = useState(DEFAULT_ITEMS);
  const [data, setData] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const debouncedSearchTerm = useDebounce(searchTerm, SEARCH_DELAY);

  useEffect(() => {
    if (searchTerm === "" || !debouncedSearchTerm) return;

    fetch(`${URL}/q=${searchTerm}`, OPTIONS)
      .then((response) => response.json())
      .then((data) =>
        data.results
          .map((item) => item.title)
          .sort()
          .slice(0, 5)
      )
      .then(setData);
  }, [searchTerm, debouncedSearchTerm]);

  useEffect(() => {
    const resultChange = (prevArr) => {
      const [firstElement, ...restElements] = prevArr;
      // If user made a search, move first element to the end of array
      // Or, get first element from search data
      const lastElement = data.length === 0 ? firstElement : data.shift();
      restElements.push(lastElement);

      return restElements;
    };

    const intervalId = setInterval(() => setSearchResults(resultChange), 1000);

    return () => clearInterval(intervalId);
  }, [data]);

  return (
    <Wrapper>
      <SearchInput
        type="search"
        placeholder="Search Google"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <ResultsList>
        {searchResults.map((item, idx) => (
          <ListItem key={idx}>
            <span>{item}</span>
          </ListItem>
        ))}
      </ResultsList>
    </Wrapper>
  );
};

export default SearchComponent;
